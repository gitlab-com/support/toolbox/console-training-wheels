# Status

Starting on 2022-06-10, the customer console and functions project will be considered in maintenance mode.
In this mode no additional functionallity will be added and any work done will be with the only purpose to fix any existing functionality.

The preferred method to add new tools/functionality will be through product feature requests.

# Review process

Due to the maintenance mode status of the project, there is a special review process in regards to reviewers and approvers.

1. Any write function changes require all configured approvals: Support Maintainer, Fulfillment Developer, and Support Manager.
1. Any read-only function changes require 1 approval: Support Maintainer.
1. All merges must be done by one of the specific Fulfillment team members who have protected branch merge rights to ensure the multi-project pipeline doesn't fail: `@ebaque @vitallium @jameslopez`
1. ReadMe changes can be made by anyone, but substantial content changes require a Support Manager approval. Due to the protected branch rules, one of the Fulfillment team members in the previous step will still have to merge.

# Usage

Usage documented in [Customer Console support workflow](https://about.gitlab.com/handbook/support/license-and-renewals/workflows/customersdot/customer_console.html).

## bin/support_console 

The modified command to start the console automatically loading the basic functions to avoid the copy/paste process we have up to this point.

## lib/support_team.rb

This file holds the functions, anything in here will be automatically loaded into the console at start by using the `support_console` alias.

## On customers.gitlab.com 

The functions file is located at `/home/customersdot/support_team.rb`
Updating this file will affect the functions listed in the [customer console workflow](https://about.gitlab.com/handbook/support/license-and-renewals/workflows/customersdot/customer_console.html). Those functions are used by support engineers and mechanizer.

Note: Use `sudo` to edit the file.
