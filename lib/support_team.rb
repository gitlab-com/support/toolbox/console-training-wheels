=begin
Group of functions for the support team to resolve requests which won't be possible to handle on customers portal.
=end

def generate_result_string ( status, message )
   puts ''
   puts ''
   { status: status, message: message }.to_json
end

# Build an Order object from zuora subscription

def create_order_from_zuora(customer_id, subscription_name)
  customer = Customer.find(customer_id)
  subscription = customer.find_active_subscription(subscription_name)
  
  if subscription
    params = {subscription_name: subscription.name, amendment_type: Amendment::Types::NEW_SUBSCRIPTION, should_notify_customer: false}
    result = CreateOrderService.new(customer).execute(params)
    generate_result_string("success", result)
  else
    generate_result_string("failed", "#{subscription_name} does not belong to customer #{customer_id}")
  end
end


# Function to see namespace information and linked orders/customer profile
# This function has not been fully updated to the mechanizer friendly format. This will likely be a chatops command soon.

def view_namespace(namespace)
  show_namespace = ['id', 'name', 'path','members_count_with_descendants', 'extra_shared_runners_minutes_limit', 'shared_runners_minutes_limit', 'billable_members_count','plan','trial_ends_on','trial']
  show_order = ['id','customer_id','subscription_id','subscription_name','start_date','end_date', 'gl_namespace_id','gl_namespace_name']
  show_customer  = ['id', 'company','first_name', 'last_name', 'email', 'uid','zuora_account_id']
  ns = Client::GitlabApp.get(Gitlab::Api::Internal::Routes.namespace_path(namespace), token: Settings.value(:gitlab_admin_api_token))

  if  ns.key?('message')
    if (ns['message'].include? "Not Found")
      return_data = generate_result_string("failed", "Unable to find namespace")
      return return_data
    end

  end

  puts "\n[+]Namespace information"
  show_namespace.each { |x| print "%-33s %s\n" % [ x, ns[x] ] }
  order = Order.find_by_gl_namespace_id ns['id']

  if ( order )
    order.respond_to?('count') ? total_orders = orders.count : total_orders = 1
    puts "[+] There are #{total_orders} orders for this namespace"
    show_order.each { |x| print " %-33s %s\n" % [ x, order[x] ] }
    customer = Customer.find(order['customer_id'])

    if ( customer )
      puts "\n[+] Customer linked to orders"
      puts " https://customers.gitlab.com/admin/customer/#{customer['id']}"
      show_customer.each { |x| print " %-33s %s\n" % [ x, customer[x] ] }
    end
  else
    if (!order)
      return_data = generate_result_string("failed", "[*] No orders found")
      return return_data
    end

  end
end

#### Find a namespace
# This has not been updated to the mechanizer friendly format (JSON). This will likely be a chatops command soon.

def find_namespace(namespace)
  ns = Client::GitlabApp.get("/api/v4/namespaces?search=#{namespace}", token: Settings.value(:gitlab_admin_api_token))

  if ns.count > 0
    print "[!] Possible matches:\n"
    ns.each do |n|
      print "\t[+] Name %s | Full Path %s\n" % [ n['name'].ljust(20), n['full_path'] ]
    end
  else
    puts "No matches (╥_╥)"
  end
  " "
end

####  Updates existing trials (or downgrades namespace) and for subscription "extensions", creates a trial if needed

def update_gitlab_plan(namespace, plan, expires = nil, sub_name = nil)
  # Get current namespace details
  ns = Client::GitlabApp.get(
    Gitlab::Api::Internal::Routes.namespace_path(namespace), token: Settings.value(:gitlab_admin_api_token)
  )

  # New plan names
  plan = case plan
    when 'gold', 'ultimate' then 'ultimate_trial'
    when 'silver', 'premium' then 'premium_trial'
    when 'bronze', 'free' then plan
    else return generate_result_string('failed', "#{plan} is not valid")
      return return_data
  end
  
  product_rate_plan_id = case plan
    when 'free' then Plan::FREE_PLAN
    when 'bronze' then Plan::BRONZE_PLAN
    when 'premium_trial' then Plan::PREMIUM_SAAS_TRIAL_PLAN
    when 'ultimate_trial' then Plan::ULTIMATE_SAAS_TRIAL_PLAN
  end

  # set minutes, matching trial minutes to paid plan since this is almost always sales requested
  plan_minutes = case plan
    when 'free' then 400
    when 'bronze' then 2000
    when 'premium_trial' then 10000
    when 'ultimate_trial' then 50000
  end

  # set extra minutes
  extra_minutes = case plan
    when 'free' then 0
    when 'bronze' then 0
    when 'premium_trial' then 10
    when 'ultimate_trial' then 10
  end

  if sub_name.present?
    sub_order = Order.where(gl_namespace_id: ns['id'], subscription_name: sub_name).last

    unless sub_order
      return generate_result_string("Failed", "Unable to find subscription #{sub_name}. This error may be due to special characters in the subscription name. Please escalate if necessary.")
    end

    if expires && sub_order.end_date >= Date.parse(expires)
      return generate_result_string("Failed", "Given end date #{expires} is earlier than (or the same as) subscription #{sub_name} end date.")
    end
  end

  order = Order.where(gl_namespace_id: ns['id'], trial: true).where.not(product_rate_plan_id: Plan::SAAS_GITLAB_DUO_PRO_TRIAL_PLAN).last
  
  # fallback lookup for old trial orders
  order ||= Order.where(gl_namespace_id: ns['id'], subscription_name: nil, subscription_id: nil).where.not(product_rate_plan_id: Plan::SAAS_GITLAB_DUO_PRO_TRIAL_PLAN).last

  # if sub exists, we can reuse that info to create a trial
  if !order && sub_order
    # create trial, and sync to .com
    trial_params = { namespace_id: ns['id'], namespace_name: ns['path'], sync_to_gl: true }
    Gitlab::HostedPlans::CreateTrialService.new(sub_order.customer, skip_plan_check: true).execute(trial_params)
    
    # Check for created trial order
    trial_order = Order.where(gl_namespace_id: ns['id'], trial: true).last

    # validating results
    if trial_order && (trial_order.end_date > Date.today)
      return generate_result_string("Success", "A 30-day trial has been created for #{namespace}. If the plan or date needs to be changed, please fill in the form again without a subscription name.")
    else
      return generate_result_string("Failed", "Unable to create a trial for #{namespace}. Check that the CustomersDot account is linked to a GitLab.com account with Owner role in the namespace.")
    end
  end

  # if still not found, assume no info for this group in CustomersDot; if we're force downgrading, we don't need an order
  if !order && plan != 'free'
    return generate_result_string("Failed", "Unable to find a trial order. Please have customer create a trial for #{namespace}.")
  end

  # Should have an existing trial order, update it
  if order.present? && plan != 'free'
    #do not modify Ultimate over Premium trials
    return generate_result_string('failed', "Refusing to modify Ultimate trial over Premium, order ID: #{order.id}") if order.ultimate_trial_paid_customer?
    
    params = { product_rate_plan_id: product_rate_plan_id, trial: true }
    params[:end_date] = Date.parse(expires) if expires

    order.update!(params)

    Gitlab::Namespaces::UpdatePlanInfoService.new(order, force_sync: true).execute
  end

  ## To downgrade. Reset trial flag
  if plan == 'free'
    #if we are here, we are wanting to set the namespace to a free plan, and ensure both trial and/or subscription end dates are no later than "today"
    params = { plan_code: Plan::FREE_PLAN, trial: false }
    today = Date.current
    
    #if either of these are in the future, set them to today instead
    trial_ends_on = [Date.parse(ns['trial_ends_on']), today].min unless ns['trial_ends_on'].nil?
    subscription_end_date = [Date.parse(ns['end_date']), today].min unless ns['end_date'].nil?

    #merge the parameters together and then push them to the namespace
    params.merge!( trial_ends_on: trial_ends_on, end_date: subscription_end_date )
    Client::GitlabApp.put("/api/v4/namespaces/#{ns['id']}/gitlab_subscription", body: params.to_json, token: Settings.value(:gitlab_admin_api_token))

    # Disable pipelines when downgrading. First check that namespace does not have purchased minutes other than the 10 we added to enable the pipelines
    if ns['extra_shared_runners_minutes_limit'] <= 10
      update_extra_minutes(namespace, extra_minutes)
    end
  end
  # update minutes quota
  Client::GitlabApp.put(
    Gitlab::Api::Internal::Routes.namespace_path(ns['id']),
    body: { shared_runners_minutes_limit: plan_minutes }.to_json,
    token: Settings.value(:gitlab_admin_api_token)
  )
  # When plan is trial, add additional minutes to enable pipelines
  if plan.include?("_trial") && (ns['extra_shared_runners_minutes_limit'] == 0 || ns['extra_shared_runners_minutes_limit'].nil?)
    update_extra_minutes(namespace, extra_minutes)
  end

  # pulling result for validation
  result = Client::GitlabApp.get(
    Gitlab::Api::Internal::Routes.namespace_path(namespace), token: Settings.value(:gitlab_admin_api_token)
  )

  # validating results
  if result['plan'] == plan && result['shared_runners_minutes_limit'] == plan_minutes && result['extra_shared_runners_minutes_limit'] >= extra_minutes
    generate_result_string('success', "#{namespace} has been updated to #{plan} with #{result['shared_runners_minutes_limit']} minutes quota and #{result['extra_shared_runners_minutes_limit']} additional minutes.")
  else
    generate_result_string('failed', "The operation was completed but #{namespace} does not show the correct plan, minutes quota, or additional minutes.")
  end
end

#### Update a group's additional minutes

def update_extra_minutes(namespace,extra_minutes)
  ns = Client::GitlabApp.get(
    Gitlab::Api::Internal::Routes.namespace_path(namespace), token: Settings.value(:gitlab_admin_api_token)
  )

  return generate_result_string("failed", "Unable to find namespace, #{namespace}") unless ns['id']

  result = Client::GitlabApp.put(
    Gitlab::Api::Internal::Routes.namespace_path(ns['id']),
    body: { extra_shared_runners_minutes_limit: extra_minutes }.to_json,
    token: Settings.value(:gitlab_admin_api_token)
  )

  if result['extra_shared_runners_minutes_limit'] > 0
    generate_result_string("success", "Minutes updated for #{namespace}.")
  else
    generate_result_string("failed", "Minutes failed to be updated for #{namespace}. Please ensure shared runners are turned on.")
  end
end

#### Update a group's monthly shared runner minutes
def update_group_mins(namespace, mins:)
  ns = Client::GitlabApp.get(
    Gitlab::Api::Internal::Routes.namespace_path(namespace), token: Settings.value(:gitlab_admin_api_token)
  )

  return generate_result_string("failed", "Unable to find namespace, #{namespace}") unless ns['id']
  Client::GitlabApp.put(
    Gitlab::Api::Internal::Routes.namespace_path(ns['id']),
    body: { shared_runners_minutes_limit: mins }.to_json,
    token: Settings.value(:gitlab_admin_api_token)
  )

  generate_result_string("success", "Group minutes updated")
end

#### Update a group's additional storage

def update_extra_storage(namespace,extra_storage)
  ns = Client::GitlabApp.get(
    Gitlab::Api::Internal::Routes.namespace_path(namespace), token: Settings.value(:gitlab_admin_api_token)
  )

  return generate_result_string("failed", "Unable to find namespace, #{namespace}") unless ns['id']

  Client::GitlabApp.put(
    Gitlab::Api::Internal::Routes.namespace_path(ns['id']),
    body: {
      additional_purchased_storage_size: extra_storage,
      additional_purchased_storage_ends_on: Date.current + 1.year
    }.to_json,
    token: Settings.value(:gitlab_admin_api_token)
  )

  generate_result_string("success", "Storage updated")
end

### Force Order to sync with what's in Zuora

def force_attr(subscription_name)
  o = Order.find_by(subscription_name: subscription_name)
  
  return generate_result_string("failed", "Unable to find order using #{subscription_name}") unless o

  subscription = o.subscription

  return generate_result_string("failed", "Unable to fetch subscription from Zuora for order #{o.id}") unless subscription

  o.update_from_subscription(subscription, {})

  Gitlab::Namespaces::UpdatePlanInfoService.new(o, force_sync: true).execute

  generate_result_string("success", "Attributes forced.")
end


### Unlink subscription with GitLab.com group
# makes use of `update_gitlab_plan` ; pulls namespace path from .com because name is not necessarily path

def unlink_sub(subscription_name)
  o = Order.find_by(subscription_name: subscription_name)

  return generate_result_string("failed", "Unable to find order using #{subscription_name}") unless o

  return generate_result_string("failed", "No namespace tied to #{subscription_name}") unless o.gl_namespace_id

  ns = Client::GitlabApp.get(
    Gitlab::Api::Internal::Routes.namespace_path(o.gl_namespace_id), token: Settings.value(:gitlab_admin_api_token)
  )

  return generate_result_string("failed", "Namespace ID #{o.gl_namespace_id} from order not found.") unless ns

  o.update!(gl_namespace_id: nil, gl_namespace_name: nil)
  order_plan = PlansFinder.find_gitlab_plan_code_by_id(o.product_rate_plan_id).to_s
  ## Find the plan codes here: https://gitlab.com/gitlab-org/customers-gitlab-com/-/blob/ce65bd37c498b549ba8c43399177ab08faf5b4e3/lib/rate_plan_helper.rb#L97
  update_gitlab_plan(ns['path'], 'free') if order_plan.in?(%w[gold ultimate silver premium bronze ultimate_trial premium_trial opensource])
  generate_result_string("success", "Subscription unlinked.")
end

# Group methods

### Force a group to be reassociated with a Subscription
### NOTE: if this fails due to CI mins already on the group (see: https://gitlab.com/gitlab-org/customers-gitlab-com/-/issues/4018)
### or something similar, a fallback method is to use: 
###
### Gitlab::SyncOrderJob.with_transaction(order: order)
###
### the Order should already have gl_namespace_id and _name set from the failed
### force_reassociation, but double-check before running

def force_reassociation(subscription_name, group_id_or_path)
   o = Order.find_by(subscription_name: subscription_name)
   ns = Client::GitlabApp.get(
     Gitlab::Api::Internal::Routes.namespace_path(group_id_or_path), token: Settings.value(:gitlab_admin_api_token)
   )

  if o.end_date.nil? || o.end_date < Date.today
    return generate_result_string("failed", "Order tied to subscription has no end date, or end date is in the past.")
  end

   #handle addons
   if Plan::GITLAB_COM_ADD_ONS.include?(o.product_rate_plan_id)
      o.update!(gl_namespace_id: ns['id'], gl_namespace_name: ns['name'])
      Gitlab::Namespaces::UpdatePlanInfoService.new(o, force_sync: true).execute

      return generate_result_string("success", "Add-on subscription associated.")
   end

   #save the current plan (in case we need to revert) then downgrade to free
   current_plan = ns['plan']
   order_plan = PlansFinder.find_gitlab_plan_code_by_id(o.product_rate_plan_id).to_s

   begin
      Client::GitlabApp.put("/api/v4/namespaces/#{ns['id']}/gitlab_subscription?plan_code=free", token: Settings.value(:gitlab_admin_api_token))
      o.update!(gl_namespace_id: ns['id'], gl_namespace_name: ns['name'])
      Gitlab::Namespaces::UpdatePlanInfoService.new(o, force_sync: true).execute

      #check if the group's new plan matches the plan in the order we just synced
      updated_ns = Client::GitlabApp.get(
        Gitlab::Api::Internal::Routes.namespace_path(ns['id']), token: Settings.value(:gitlab_admin_api_token)
      )

      if updated_ns['plan'] == order_plan
        generate_result_string("success", "Association forced.")
      else
        generate_result_string("failed", "Association forced, but group does not show new plan")
      end
  rescue => e
    #revert back to current_plan
    Client::GitlabApp.put("/api/v4/namespaces/#{ns['id']}/gitlab_subscription?plan_code=#{current_plan}", token: Settings.value(:gitlab_admin_api_token))

    generate_result_string("failed", "There was an error: #{e.message}")
  end
end

### Completely unlink a GitLab.com account from a customers account

def unlink_customer(customer_id)
   c = Customer.find_by(id: customer_id)
   return generate_result_string("failed", "#{customer_id} does not match any known Customer record.") unless c
   c.update!(access_token: nil, uid: nil, provider: nil, refresh_token: nil, token_refresh_last_attempted_at: nil, access_token_expires_at: nil)
   generate_result_string( "success", "Customer unlinked" )
end

### Associate full user count with group

def associate_full_user_count_with_group(subscription_name)
    order = Order.find_by(subscription_name: subscription_name)
    return generate_result_string("failed", "#{subscription_name} does not match any known Order") unless order
    
    count = order.subscription.products.map{ |p| p.trueup? ? 0 : p.quantity }.sum

    plan = SubscriptionParamsGenerator.plan_id_to_plan_code(order.product_rate_plan_id)

    subscription_params =
     {
       start_date: order.start_date,
       end_date: order.end_date,
       plan_code: plan,
       seats: count,
       trial: false
     }

    Gitlab::Subscription::UpdateService.new(order.gl_namespace_id, subscription_params).execute
    generate_result_string("success", "Updated seats to #{count}")
end

# Reset the Max Seats Used number directly in GitLab.com

def account_seats(namespace, max = nil)
  ns = Client::GitlabApp.get(
    Gitlab::Api::Internal::Routes.namespace_path(namespace), token: Settings.value(:gitlab_admin_api_token)
  )
  subscription_api = Gitlab::Api::Internal::Routes.subscription_path(ns['id'])
  seats_info = Client::GitlabApp.get(subscription_api, token: Settings.value(:gitlab_admin_api_token))

  if not ns
    generate_result_string("failure","Cannot find the given group")
  end

  if not seats_info
    generate_result_string("failure","Cannot find a subscription for the given group")
  end

  if !max.nil?
    put_subscription_api = subscription_api + "?max_seats_used=#{max}"
    update_seats_info = Client::GitlabApp.put(put_subscription_api, token: Settings.value(:gitlab_admin_api_token))
    generate_result_string("success", "Seats updated and will be automatically recalculated at 12:00 UTC")
  end
  nil
end

#### Validate group of users for shared runner credit card directly

def group_cc_validation(group)
  page = 1
  per_page = 20
  activated_cc_members = 0
  response = { 'status' => 'failed' , 'message' => ''}
  group_object = Client::GitlabApp.get("/api/v4/groups/#{group}", token: Settings.value(:gitlab_admin_api_token))
  
  if not group_object
    generate_result_string("failure","Cannot find the given group")
  end

  group_members_object = Client::GitlabApp.get("/api/v4/groups/#{group_object['id']}/members/all", token: Settings.value(:gitlab_admin_api_token))
  total_members = group_members_object.headers['x-total'].to_i
  

  while ( activated_cc_members < total_members )
    group_members_object = Client::GitlabApp.get("/api/v4/groups/#{group_object['id']}/members/all?page=#{page}&per_page=#{per_page}", token: Settings.value(:gitlab_admin_api_token)) 
    group_members_object.each { |m| puts validate_my_cc m['username'] }
    page=page+1
    activated_cc_members = activated_cc_members + per_page
  end

  generate_result_string("Success","Validated all group members")

end


### Function to remove the CC validation for sales supported trials

def enable_ci_minutes(namespace)
  ns = Client::GitlabApp.get(
    Gitlab::Api::Internal::Routes.namespace_path(namespace), token: Settings.value(:gitlab_admin_api_token)
  )
  current_plan = ns['plan']
  
  if ns['trial_ends_on'].present?
    on_trial = Date.parse(ns['trial_ends_on']) >= Date.current
    return generate_result_string("failed", "Looks like #{namespace} has an expired trial") unless on_trial
  elsif current_plan.include? "_trial"
    update_gitlab_plan(namespace, 'free')
    return generate_result_string("failed", "#{namespace} was on trial without an end date and has been downgraded to Free.")
  else
    return generate_result_string("failed", "Not on trial or missing trial end date. Please review the plan and date for #{namespace}.")
  end
  
  if current_plan.include? "_trial"
    if ns['extra_shared_runners_minutes_limit'] == 0 || ns['extra_shared_runners_minutes_limit'].nil?
      update_extra_minutes(namespace, 10)
    else
      return generate_result_string("failed", "#{namespace} has existing additional minutes and should not encounter credit card validation.")
    end
  end
  
  result = Client::GitlabApp.get(
    Gitlab::Api::Internal::Routes.namespace_path(namespace), token: Settings.value(:gitlab_admin_api_token)
  )

  if result['extra_shared_runners_minutes_limit'].to_i > 0
    generate_result_string("success", "Added additional minutes to #{namespace} to enable running CI without CC validation.")
  else
    generate_result_string("failed", "Additional minutes were not added to #{namespace}.")
  end
end

### Function to calculate the storage usage of the container registry for each project in a given group
## Note: For a large group, this may take several hours.

def container_registry_storage_usage(group_name)

  group_info = Client::GitlabApp.get("/api/v4/groups/#{group_name}", token: Settings.value(:gitlab_admin_api_token))

  return 0 unless group_info['id']
  group_id = group_info['id']
  group_query = Client::GitlabApp.get("/api/v4/groups/#{group_id}/registry/repositories?tags=1", token: Settings.value(:gitlab_admin_api_token))
  total_pages = group_query.headers['x-total-pages'].to_i
  current_page = 1
  total_size_per_project = {}

  total_storage = 0

  for i in 1..total_pages

    gq = Client::GitlabApp.get("/api/v4/groups/#{group_name}/registry/repositories?tags=1&per_page=100&page=#{current_page}", token: Settings.value(:gitlab_admin_api_token))

    gq.each do |repo|
      repo_id = repo['id']
      project_id = repo['project_id']

      project_info = Client::GitlabApp.get("/api/v4/projects/#{project_id}", token: Settings.value(:gitlab_admin_api_token))
      project_name = project_info['name']
      project_path = project_info['path_with_namespace']
      total_size_per_project[project_name] = {}
      total_size_per_project[project_name]['project_id'] = project_id
      total_size_per_project[project_name]['project_path'] = project_path
      total_size_per_project[project_name]['container_registry_storage_used'] = 0


      repo['tags'].each do |t|
        tag_name =  t['name']
        tag = Client::GitlabApp.get("/api/v4/projects/#{project_id}/registry/repositories/#{repo_id}/tags/#{tag_name}", token: Settings.value(:gitlab_admin_api_token))
        total_size_per_project[project_name]['container_registry_storage_used'] = total_size_per_project[project_name]['container_registry_storage_used'] + tag['total_size'].to_f
      end
      total_size_per_project[project_name]['container_registry_storage_used'] = total_size_per_project[project_name]['container_registry_storage_used'] / (1024 * 1024 * 1024)
      total_storage = total_storage + total_size_per_project[project_name]['container_registry_storage_used']
      total_size_per_project[project_name]['container_registry_storage_used'] = total_size_per_project[project_name]['container_registry_storage_used'].round(3).to_s + " GB"

    end
    current_page = current_page + 1
  end
  print "[*] Total Storage Used [ %s GB ]  \n\n" % total_storage.round(3).to_s
  pp total_size_per_project
end

### Function to create a license for any emergency situation during the weekend.
### The license will be valid for 5 days and the number of seats can be adjusted
## This is not intended to be used as a replacement from customerDot web UI.

def create_trial_license(license_user_count, email)
  #raise 'License user count must be a positive number' unless license_user_count > 0
  license_user_count = license_user_count.to_i
  generate_result_string('Failure', 'License user count must be positive') if license_user_count < 1

  l = License.create!(
    name: '10-day Trial GitLab License',
    company: '10-day Trial GitLab License',
    email: email,
    users_count: license_user_count,
    duration_days: 10,
    plan_code: ::Plan::ULTIMATE,
    trial: true,
    skip_trial_validation: true
  )
  ## Sends the formated email with the license
  LegacyLicenseNotificationService.new(l, email).execute

  generate_result_string('Success', "Trial license created for #{email}, License #{l.license_file}")
end
